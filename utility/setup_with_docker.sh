#!/usr/bin/env bash
set -e
cmd="$@"

echo "Installing docker..."
wget -qO- https://get.docker.com/ | sh
sudo apt-get install -y python-pip
sudo pip install docker-compose
docker-compose -v
docker -v

echo "Building app..."
sudo docker-compose build

echo "Running app in detached mode..."
sudo docker-compose up -d

