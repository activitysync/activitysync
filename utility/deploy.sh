#!/bin/bash
set -e
cmd="$@"
cd activitysync
git pull
sudo docker-compose build django
sudo docker-compose up --no-deps -d django

