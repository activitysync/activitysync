#!/usr/bin/env bash
source ../../.env

sudo apt-get install mysql-server libmysqlclient-dev
sudo mysql_install_db
sudo mysql_secure_installation

echo "Creating database..."
mysql --user="$MYSQL_ROOT_USER" --password="$MYSQL_ROOT_PASSWORD" --execute="CREATE DATABASE $MYSQL_DATABASE CHARACTER SET UTF8;"
mysql --user="$MYSQL_ROOT_USER" --password="$MYSQL_ROOT_PASSWORD" --execute="CREATE USER $MYSQL_USER@localhost IDENTIFIED BY '${MYSQL_PASSWORD}';"
mysql --user="$MYSQL_ROOT_USER" --password="$MYSQL_ROOT_PASSWORD" --execute="GRANT ALL PRIVILEGES ON $MYSQL_DATABASE.* TO $MYSQL_USER@localhost;"
mysql --user="$MYSQL_ROOT_USER" --password="$MYSQL_ROOT_PASSWORD" --execute="FLUSH PRIVILEGES;"
