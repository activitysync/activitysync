(function () {
    'use strict';

    angular
        .module('activitysync')
        .run(function ($log) {
            $log.debug('runBlock end')
        });

})();
