(function () {
    'use strict';

    describe('controllers: MainController', function () {
        beforeEach(module('activitysync'));

        var $scope = null;
        var ctrl = null;

        beforeEach(inject(function ($rootScope, $controller) {
            $scope = $rootScope.$new();
            ctrl = $controller('MainController', {$scope: $scope});
        }));

        describe('[fn] $scope.stringLength', function () {

            it('sets the len to "long" if the str length is > 8 chars', function () {
                ctrl.str = 'verylongsamplestring';
                ctrl.stringLength();
                expect(ctrl.len).toEqual('long');
            });

            it('sets the len to "short" if the str length is <= 8 chars', function () {
                ctrl.str = 'shortstr';
                ctrl.stringLength();
                expect(ctrl.len).toEqual('short');
            });

        });

    });
})();
