(function () {
    'use strict';

    angular
        .module('activitysync')
        .controller('MainController', function ($http, $log, moment) {

            var vm = this;

            vm.events = [];
            vm.disciplines = [{uniqueID: 1, name: "football"}, {uniqueID: 2, name: "basketball"}];

            vm.sportEvent = {
                disciplineID: 0,
                numberOfPlayers: "",
                since: moment(),
                to: moment().add(1, 'days'),
                freePlaces: "",
                address: "",
                level: ""
            };

            vm.getEvents = function () {

                $http.post("/rest/events/find", {
                    disciplineID: vm.sportEvent.disciplineID,
                    since: moment(vm.sportEvent.since),
                    to: moment(vm.sportEvent.to),
                    address: vm.sportEvent.address.toString() || null,
                    numberOfPlayers: parseInt(vm.sportEvent.numberOfPlayers) || null,
                    freePlaces: parseInt(vm.sportEvent.freePlaces) || null,
                    level: vm.sportEvent.level
                }).then(function (result) {
                    $log(result.data);
                    vm.events = result.data;
                });

            };

            vm.setNow = function () {
                vm.sportEvent.since = moment();
            };
            vm.setDayAgo = function () {
                vm.sportEvent.since = moment().subtract(1, 'days');
            };
            vm.setWeekAgo = function () {
                vm.sportEvent.since = moment().subtract(1, 'weeks');
            };
            vm.setDay = function () {
                vm.sportEvent.to = moment().add(1, 'days');
            };
            vm.setWeek = function () {
                vm.sportEvent.to = moment().add(1, 'weeks');
            };
            vm.setMonth = function () {
                vm.sportEvent.to = moment().add(1, 'months');
            };

            // Sample data for unit test

            vm.str = '';
            vm.stringLength = function () {
                vm.len = (vm.str.length > 8) ? 'long' : 'short';
            };

        });
})();
