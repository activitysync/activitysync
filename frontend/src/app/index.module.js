(function () {
    'use strict';

    angular
        .module('activitysync', [
            'ngAnimate',
            'restangular',
            'ui.router',
            'ngMaterial',
            'toastr',
            'ngMaterialDatePicker'
        ]);

})();
