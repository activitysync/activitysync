#!/bin/bash
GREEN='\033[0;32m'
NOCOLOR='\033[0m'

# update package lists from repositories
printf "${GREEN}[Update package lists from repositories]${NOCOLOR}\n"
sudo apt-get update

# git installation
printf "${GREEN}[GIT installation]${NOCOLOR}\n"
sudo apt-get install git

# nodejs installation
printf "${GREEN}[node.js installation]${NOCOLOR}\n"
sudo apt-get install nodejs

# npm installation
printf "${GREEN}[NPM installation]${NOCOLOR}\n"
sudo apt-get install npm

# nodejs-legacy package installs a node symlink that is needed by many modules to build and run correctly
printf "${GREEN}[nodejs-legacy installation]${NOCOLOR}\n"
sudo apt-get install nodejs-legacy

# bower installation
printf "${GREEN}[Bower installation]${NOCOLOR}\n"
sudo npm install -g bower

# gulp installation
printf "${GREEN}[gulp-cli installation]${NOCOLOR}\n"
sudo npm install -g gulp-cli

printf "${GREEN}Setup Configuration DONE${NOCOLOR}\n"
