#!/bin/bash
GREEN='\033[0;32m'
NOCOLOR='\033[0m'

# npm packages installation
printf "${GREEN}[NPM installation]${NOCOLOR}\n"
sudo npm install

# bower packages installation
printf "${GREEN}[Bower installation]${NOCOLOR}\n"
sudo bower install --allow-root

printf "${GREEN}Setup Packages DONE${NOCOLOR}\n"
