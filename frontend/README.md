# ActivitySync-Frontend (Angular.js)
App for organizing sport events

## Preparing developer's environment
To prepare developer's environment follow the quick guide below. Stay responsive during script execution and be patient - it takes time.

### Linux

1. Make sure you have clean [Ubuntu][ubuntu] installation - Ubuntu 14.04.5 LTS (Trusty Tahr) 64-bit

2.  Download and run script by executing following commands (do not run script with super user context!)

    ```
    wget https://gitlab.com/activitysync/activitysync/raw/master/frontend/setup_environment.sh
    sh setup_environment.sh
    ```

3. Clone repository

    ```
    git clone https://gitlab.com/activitysync/activitysync.git
    ```

4. Go to <project_files>/frontend directory and run script (installation of npm and bower packages) by executing the following commands
    ```
    cd activitysync/frontend
    sh setup_packages.sh
    ```

### Windows

1. Install
    * [node.js][nodejs]
    * [git][git]
    * bower (by executing the following command)
        ```
        npm install -g bower
        ```

2. Clone repository
    ```
    git clone https://gitlab.com/activitysync/activitysync.git
    ```

3. Go to <project_files>/frontend directory and run commands (installation of npm and bower packages)
    ```
    npm install
    bower install
    ```


## Usage 

Go to <project_files>/frontend directory

### Commands

* To launch a server which supports live reload of your modifications use:
   ```
   gulp serve
   ```
* To launch tests use:
   ```
   gulp test
   ```
* To start optimization process and create minimized files use (output in /dist directory):
   ```
   gulp
   ```

## Licenses

Project is build from [generator-gulp-angular][generator].

[nodejs]: <https://nodejs.org/en/download/>
[git]: <https://git-for-windows.github.io/>
[generator]: <https://github.com/Swiip/generator-gulp-angular>
[ubuntu]: <http://www.ubuntu.com/download/alternative-downloads>
